# darkDashboard
A glass website, in form of a game dashboard that I've designed using only HTML &amp; CSS, in dark mode

Here's the preview of the web page

![image](https://user-images.githubusercontent.com/39695220/113510387-47e90600-9578-11eb-8e34-c90da3ad2fff.png)

